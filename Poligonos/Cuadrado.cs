﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poligonos
{
    public class Cuadrado
    {
        /// <summary>
        /// propiedad lado con su constructor
        /// </summary>
        public double lado { get; set; }
        /// <summary>
        /// propiedad area de solo lectura cuyo constructor es la propiedad lado elevada al cuadrado
        /// </summary>
        public double area { get => lado * lado; }
    }
}
